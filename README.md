# KCK Icons Repository

A small collection of free license icons in various formats used in some of my web projects.

Install with the command:

`git clone https://bitbucket.org/frabjous/icons.git`

My web projects that use this repo expect it to be in a direct subfolder named `/icons` in the main document root of the webserver.